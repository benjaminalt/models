#!/bin/bash
# Author: Bernd Eckstein, Luc Guyot
#
# This script reads models from _rpmbuild/models.txt
# and creates symlinks in ~/.gazebo/models
# It flattens models located in subdirectories

function log() {
    echo "[HBP-NRP] $(date --rfc-3339=seconds) $*"
}


function create_symlinks_for_models() {
    echo
    echo "--------------------------------------------------------------------------------------------------"
    echo "  Creating symlinks to ~/.gazebo/models and to ${HBP}/gzweb/http/client/assets"
    echo "--------------------------------------------------------------------------------------------------"
    echo 
    local DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$1
    [[ "$1" != "" ]] && MODELS=$(ls "${DIR}") || MODELS=$(cat "${DIR}/_rpmbuild/models.txt")
    for model in ${MODELS[@]}; do
        model_basename=$(basename ${model})
        gazebo_linkname=~/.gazebo/models/${model_basename}
        gzweb_linkname=${HBP}/gzweb/http/client/assets/${model_basename}
        log "Creating two links for ${model} (former links are overwritten)"
        model_dir=${DIR}/${model}
        rm -rf ${gazebo_linkname} ${gzweb_linkname}
        ln -s ${model_dir} ${gazebo_linkname}
        ln -s ${model_dir} ${gzweb_linkname}
    done
}


usage()
{
cat << EOF
Without options, this will create symlinks for models located in $HBP/Models
OPTIONS:
   -h      Show this message
   -d      Create symlinks for models located in the specified directory
EOF
exit
}

opt=$1
shift
case ${opt} in
    -d)
        echo "Create symlinks for every model folder located in $1"
        echo
        ;;
    "")
        echo "Create symlinks for every model folder located in ${HBP}/Models"
        echo 
        ;;
    *)
       usage
       ;;
esac

create_symlinks_for_models $1

