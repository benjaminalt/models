#!/bin/bash

DIR="$( cd "$( dirname "$0" )" && pwd )"

start() {
    python $DIR/controller.py &
}

stop() {
    # Only one controller can run at a time
    ps -Af | grep controller.py | grep icub_model | awk '{print $2}' | xargs kill -9
}

mode=$1

case $mode in
'start')
    start
    exit 0
    ;;
'stop')
    stop
    exit 0
    ;;
*)
    echo "Unknown mode $mode."
    exit 1
    ;;
esac
