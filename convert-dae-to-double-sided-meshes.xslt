<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:collada="http://www.collada.org/2005/11/COLLADASchema">
    <xsl:output indent="yes"/>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="collada:library_effects/collada:effect/collada:profile_COMMON[not(collada:extra)]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="node()"/>
            <extra xmlns="http://www.collada.org/2005/11/COLLADASchema">
                <technique profile="threejs">
                    <double_sided>1</double_sided>
                </technique>
            </extra>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>