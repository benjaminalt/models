cmake_minimum_required(VERSION 2.8.3)
project(model_template)

install(
  PROGRAMS src/hbp_create_model
  DESTINATION /usr/bin
)

install(
  DIRECTORY src/model_template
  DESTINATION /usr/share/hbp_templates
)